# from modules.crawler import RockAutoCrawler;
# from modules.crawler import DiscountBodyPartsCrawler;
from modules.crawler import CaridCrawler;
from modules.dbFileCreator.CaridDbCreator import CaridDbCreator;

caridCrawlerLink = "https://www.carid.com";
caridCrawlerQuery = caridCrawlerLink;

year = "1999";
make = "toyota";
model = "supra";

caridCrawlerQuery += "/" + year + "-" + make + "-" + model + "-accessories/";

print caridCrawlerQuery;

caridCrawler = CaridCrawler(caridCrawlerQuery,caridCrawlerLink,year,make,model);

caridCrawler.initFields();
caridCrawler.digIntoCraid();

partTitles = caridCrawler.getPartTitles();

caridCrawler.generatePartDictionary();



caridDbCreator = CaridDbCreator(caridCrawler.getPartDictionaryList(),partTitles, caridCrawler.getSearchTerms(),True);

caridDbCreator.populateTables();
