from ..parent.Crawler import Crawler;
from utils.ArrayFunctions import ArrayFunctions;
import time;
import sys;

class CaridCrawler(Crawler):
    def __init__(self,path,main_url,year,make,model):
        super(CaridCrawler, self).__init__(path);
        self.__main_url = main_url;
        self.__partDictionary = []
        self.__searchTerms = {
            'year': year,
            'make': make,
            'model': model
        }

    def getSearchTerms(self): return self.__searchTerms;

    def getPartDictionaryList(self): return self.__partDictionary;

    def initFields(self):
        self.__partDictionary = self.getPartComponentDictionary();

    # Iterates through each of the body parts links and retrieves the contents
    def digIntoCraid(self):

        for category in self.__partDictionary:
            time.sleep(2)
            category['parser'] = self._BeautifulSoup(self._openLink(self.__main_url + category['link']),'html5lib');
            time.sleep(2);
    

    # Based on the contents retreived this creates a dictionary of each product
    def generatePartDictionary(self):
        for category in self.__partDictionary:
            productList = self._getMainProductList(category['parser']);
            category['productTableData'] = ArrayFunctions.map(self._createProductDictionary,productList);


    # returns the list of all the li's parsed out from the main prod list
    def _getMainProductList(self,parser):
        return parser.find(class_ = "js-main-prod-list").find_all('li');

    # Parses through the product pulling out relvant info to create a table
    def _createProductDictionary(self, product):
        imgsrc = "";
        if product.find(class_ = "lst_ic_h").find('noscript'):
            versionArr = sys.version_info;
            if versionArr[0] == 2 and versionArr[1] == 7:
                if versionArr[2] == 10:
                    imgsrc = product.find(class_="lst_ic_h").find('noscript').img.attrs.get('src').encode(
                        'utf-8').strip();
                if versionArr[2] == 6:
                    imgTagStr = product.find(class_="lst_ic_h").find('noscript').string.encode('utf-8').strip();
                    imgsrc = self._BeautifulSoup(imgTagStr, 'html5lib').find('img').attrs.get('src').encode(
                        'utf-8').strip();
        else :
            imgsrc = product.find(class_ = "lst_ic_h").find('img').attrs.get('src').encode('utf-8').strip();

        mainBody = product.find(class_ = "lst_main");

        # The code is broken up into right and left part
        leftpart = mainBody.find(class_ = 'lst-info-left-part');
        rightpart = mainBody.find(class_ = 'lst-info-right-part');

        linkBlock = leftpart.find('a');
        descriptionBlock = leftpart.find(class_ = 'lst-descr');

        link = linkBlock.attrs.get('href').encode('utf-8').strip();
        company = linkBlock.find(class_ = "lst_a_name").find('b').string.encode('utf-8').strip();
        item = linkBlock.find(class_ = "lst_a_name").find('span').string.encode('utf-8').strip();

        description = descriptionBlock.find(class_ = 'lst-descr-text').string.encode('utf-8').strip();

        # There is a list of features that need to be parsed later
        features = [];
        if len(descriptionBlock.find_all(class_ = "lst_features")) != 0:
            features = descriptionBlock.find(class_ = "lst_features").find_all('span');

        features = ArrayFunctions.reduce(self.__combineAllFeatures,ArrayFunctions.map(self.__createFeatureStr,features),"");

        price = rightpart.find(class_ = "product-list-qty-form-holder").find("span").string.encode('utf-8').strip();

        return {
            "imgsrc": self.__main_url + imgsrc,
            "link": self.__main_url + link,
            "company": company.replace("\"","'"),
            "item": item.replace("\"","'"),
            "description": description.replace("\"","'"),
            "features": features.replace("\"","'") if features != None else "",
            "price": price
        }

    def __combineAllFeatures(self,itemA,itemB):
        return str(itemA) + "," + str(itemB);

    def __createFeatureStr(self,feature):
        return feature.string.encode('utf-8').strip();

    # returns list of body part titles
    def getPartTitles(self):
        return self._getAllTitles(self.__partDictionary);


    def printOutWebPage(self):
        print self._parser;


    # finds and returns the autobody block from the page
    def _findAutoBodyBlock(self): return self._parser.find(id="spb_autobodyparts");

    # Returns a dictionary that contains the title and links associated with the body parts block
    def getPartComponentDictionary(self):
        return ArrayFunctions.map(self._getLinksAndTitles,self._findAutoBodyBlock().find_all('li'));


    # Retrieves the link and title for all the sub components under auto body part
    def _getLinksAndTitles(self,item):
        title = item.find('b').string.encode('utf-8').strip();
        # Links replace the query at the end of the website
        link = item.find('a').attrs.get('href').encode('utf-8').strip();
        return {
            "title": title.lower().replace(" ","_"),
            "link": link
        }

    # Returns all titles in the map
    def _getAllTitles(self,map):
        return ArrayFunctions.map(self._stripTitlesFromMap,map);

    # strips title from each item in the map
    def _stripTitlesFromMap(self,item):
        return item['title'];
