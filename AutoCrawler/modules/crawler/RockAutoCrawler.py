from ..parent.Crawler import Crawler;
from utils.ArrayFunctions import ArrayFunctions;


# Rock auto website is going to be a very complicated scrape

class RockAutoCrawler(Crawler):
    def __init__(self,path):
        assert(path.find('rockauto.com') > 0), 'This is not a link for rock auto';
        super(RockAutoCrawler,self).__init__(path);


    def printAllHtml(self):
        print self._parser;