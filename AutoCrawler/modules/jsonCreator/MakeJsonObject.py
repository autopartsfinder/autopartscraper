import os;
import json;
import csv;

class MakeJsonObject:
    def __init__(self,location,tableHeader,dbName):
        # csv file location
        self.__location = location;
        # list of files in the directory
        self.__listOfFiles = os.listdir(self.__location +"/");
        # header for the table
        self.__tableHeader = tableHeader;
        # title aka file name
        self.__dbName = dbName;
        self.__jsonDumpLocation = "/Users/xavierthomas/pythonProjects/autopartscraper/AutoCrawler/modules/jsonDump";
        self.jsonFileName = self.__jsonDumpLocation + "/caridDb.json";
        self.createMainJson();
        self.mainFileData = {};

    def createMainJson(self):
        mainFile ={
            self.__dbName: {}
        }
        with open(self.jsonFileName,'w') as jsonMainFile:
            json.dump(mainFile,jsonMainFile);

    def loadsUpMainJsonFile(self):
        with open(self.jsonFileName) as json_file:
            self.mainFileData = json.load(json_file)

    def updateMainFileDataJson(self):
        count = 0;
        for file in self.__listOfFiles:
            count += 1;
            reader = csv.DictReader(open(self.__location + "/" + file, 'r'),delimiter="|")
            tableName = file.split(".")[0]
            self.mainFileData[self.__dbName][tableName] = []
            for row in reader:
                self.mainFileData[self.__dbName][tableName].append(row)
        with open(self.jsonFileName,'w') as json_file:
            json.dump(self.mainFileData, json_file);
