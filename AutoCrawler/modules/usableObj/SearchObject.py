class SearchObject:
    def __init__(self,carName,year,make,model):
        self.__carName = carName;
        self.__year = year;
        self.__make = make;
        self.__model = model;


    def getCarName(self): return self.__carName;

    def getYear(self): return self.__year;

    def getMake(self): return self.__make;

    def getModel(self): return self.__model;