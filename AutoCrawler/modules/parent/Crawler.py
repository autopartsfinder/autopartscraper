from bs4 import BeautifulSoup;
from utils import ArrayFunctions;

import urllib2;
import json;

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8'}

# parent class for all of the crawlers
class Crawler(object):
    def __init__(self, path):
        super(Crawler, self).__init__();
        # internal use vars
        self._path = path;

        # EXTERNAL DEPENDENCIES

        # Python library for pulling data out of HTML and XML files
        self._BeautifulSoup = BeautifulSoup;
        # Defines functions and classes that help in opening urls
        self._urllib2 = urllib2;
        # json encoder and decoder
        self._json = json;

        # Setting up main vars

        self.html = self._openLink(path);
        self._parser = self._BeautifulSoup(self.html,'html5lib');




    def getSearchTerms(self): return tuple(self._searchTerms);

    def getPath(self): return self._path;

    def _openLink(self,path):
        req = urllib2.Request(path,headers=hdr)
        return self._urllib2.urlopen(req).read();