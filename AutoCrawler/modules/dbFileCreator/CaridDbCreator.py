import os;
from datetime import datetime
from ..jsonCreator import MakeJsonObject
import json;
from utils import ArrayFunctions;


# For our purposes a db is a folder
# the table is the files in the folder
class CaridDbCreator:
    def __init__(self, partList, listOfTables, searchTerms, createNewDb):
        self.__partList = partList;
        self.__listOfTables = listOfTables;
        # Stores the year make and model
        self.__searchTerms = searchTerms;

        self.__dbName = "CaridDb";
        # Will hold the links for each table
        self.__fileDictionary = {}

        self.__dbLocation = os.getcwd() + "/modules/fileDb/" + self.__dbName;

        if createNewDb:
            # checks if the db folder already exists
            if os.access(self.__dbLocation, os.F_OK):
                os.rename(self.__dbLocation, self.__dbLocation + datetime.now().strftime('%Y-%m-%d-%H:%M:%S'));
                os.mkdir(self.__dbLocation);
            else:
                os.mkdir(self.__dbLocation);

        for table in listOfTables:
            tableName = table.lower().replace(" ","_");
            self.__fileDictionary[tableName] = {};
            self.__fileDictionary[tableName]['filePath'] = self.__dbLocation + "/" + tableName + ".csv";

    def populateTables(self):
        tableHeader = "year|make|model|imgsrc|link|company|item|description|features|price|id\n";

        for part in self.__partList:
            count = 0;
            fileTable = open(self.__fileDictionary[part['title']]['filePath'], "a+");

            if (fileTable.readline().strip() != tableHeader[0:len(tableHeader) - 1]):
                # writes the header
                fileTable.write(tableHeader);

            concatinatedStr = "";

            # Product is a dictionary that contains a key value pair where the keys match the tableheader 
            for product in part['productTableData']:
                concatinatedStr += self.__orderDataValues(product, count);
                count += 1;

            # fileTable.write(json.dumps(tableDictionary));
            fileTable.write(concatinatedStr);
            fileTable.close();

        createJsonFile = MakeJsonObject(self.__dbLocation, tableHeader, self.__dbName)
        createJsonFile.loadsUpMainJsonFile();
        createJsonFile.updateMainFileDataJson()

    def __orderDataValues(self, product, count):
        return self.__searchTerms['year'] + "|" + self.__searchTerms['make'] + "|" + self.__searchTerms['model'] + "|" \
               + self.__resolveNoneIssues(product['imgsrc']) + "|" + self.__resolveNoneIssues(product['link']) + "|" \
               + self.__resolveNoneIssues(product['company']) + "|" + self.__resolveNoneIssues(product['item']) + "|" \
               + self.__resolveNoneIssues(product['description']) + "|" + self.__resolveNoneIssues(product['features']) \
               + "|" + self.__resolveNoneIssues(product['price']) + "|" + str(count) + "\n";

    def __resolveNoneIssues(self, item):
        if item is not None:
            return item;
        return "null";
